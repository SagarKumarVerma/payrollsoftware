﻿using Payroll.Models;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Text;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;
using System.Web.Mvc.Html;

namespace Payroll.Controllers
{
    public class FormulaSetupController : Controller
    {
        // GET: FormulaSetup
        string APIUrl = ConfigurationManager.AppSettings["APIURL"].ToString();
        public ActionResult FormulaSetup()
        {
            try
            {
                return View();
            }
            catch (Exception ex)
            {
                ExceptionLogging.SendErrorToText(ex);
                ViewBag.Message = "Error occured, contact to administrator";
                return View();
            }
        }

        //// Get all Formula Code
        public JsonResult ListAllFormulaCode()
        {
            FormulaSetupModel FrmlStpMdl = new FormulaSetupModel();
            List<FormulaSetupModel.FormulaCode_list> lst = new List<FormulaSetupModel.FormulaCode_list>();
            try
            {
                string userName = Session["UserName"].ToString();
                FrmlStpMdl.LastModifiedBy = userName;
                FormulaSetupModel.FormulaCode_list FrmlStpCode = new FormulaSetupModel.FormulaCode_list();
                using (var client = new HttpClient())
                {
                    //Passing service base url  
                    client.BaseAddress = new Uri(APIUrl);
                    client.DefaultRequestHeaders.Clear();
                    client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
                    //Sending request to find web api REST service resource GetAllEmployees using HttpClient  
                    var res = client.PostAsJsonAsync("FormulaSetup/GetFormulaCodeList", FrmlStpMdl).Result;

                    //Checking the response is successful or not which is sent using HttpClient  
                    if (res.IsSuccessStatusCode)
                    {
                        var lstResponse = res.Content.ReadAsStringAsync().Result;
                        //Deserializing the response recieved from web api and storing into the Employee list  
                        lst = JsonConvert.DeserializeObject<List<FormulaSetupModel.FormulaCode_list>>(lstResponse);
                        if (lst.Count > 0)
                        {
                            foreach (FormulaSetupModel.FormulaCode_list Fsm in lst)
                            {
                                FrmlStpCode.FrmlCode = Fsm.FrmlCode;
                                FrmlStpCode.FrmlFlag = Fsm.FrmlFlag;

                            }
                            return Json(lst.ToList(), JsonRequestBehavior.AllowGet);
                        }
                    }
                    else
                    {
                        return Json(AppConstant.JSONERRORLOGMSG, JsonRequestBehavior.AllowGet);
                    }
                }
                return Json(lst.ToList(), JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                ExceptionLogging.SendErrorToText(ex);
                ViewBag.Message = "Error occured, contact to administrator";
                return Json(AppConstant.JSONERRORLOGMSG, JsonRequestBehavior.AllowGet);
            }

        }
        public JsonResult List()
        {
            try
            {
                FormulaSetupModel FormulaModel = new FormulaSetupModel();
                List<FormulaSetupModel> lst = new List<FormulaSetupModel>();
                FormulaModel.LastModifiedBy = Session["UserName"].ToString();
                FormulaModel.LoginTerminalNameIP = Session["LoginIP"].ToString() + " " + Session["LoginTerminalName"].ToString();

                using (var client = new HttpClient())
                {
                    client.BaseAddress = new Uri(APIUrl);
                    var res = client.PostAsJsonAsync("FormulaSetup/GetAllList", FormulaModel).Result;
                    if (res.IsSuccessStatusCode)
                    {
                        var lstResponse = res.Content.ReadAsStringAsync().Result;
                        lst = JsonConvert.DeserializeObject<List<FormulaSetupModel>>(lstResponse);
                    }
                }
                return Json(lst, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                ExceptionLogging.SendErrorToText(ex);
                return Json(AppConstant.JSONERRORLOGMSG, JsonRequestBehavior.AllowGet);
            }

        }
        //Get all FormulaSetup Master for jquery data table
        [HttpPost]
        public ActionResult GetAllFormulaSetup()
        {
            FormulaSetupModel FrmlSetupMst = new FormulaSetupModel();
            List<FormulaSetupModel> lst = new List<FormulaSetupModel>();
            try
            {
                string userName = Session["UserName"].ToString();
                FrmlSetupMst.LastModifiedBy = userName;
                string draw = Request.Form.GetValues("draw")[0];
                string order = Request.Form.GetValues("order[0][column]")[0];
                string orderDir = Request.Form.GetValues("order[0][dir]")[0];
                int startRec = Convert.ToInt32(Request.Form.GetValues("start")[0]);
                int pageSize = Convert.ToInt32(Request.Form.GetValues("length")[0]);
                string search = Request.Form.GetValues("search[value]").FirstOrDefault();

                ///

                ///
                using (var client = new HttpClient())
                {

                    client.BaseAddress = new Uri(APIUrl);

                    var res = client.PostAsJsonAsync("FormulaSetup/GetAllList", FrmlSetupMst).Result;

                    //Checking the response is successful or not which is sent using HttpClient  
                    if (res.IsSuccessStatusCode)
                    {

                        var lstResponse = res.Content.ReadAsStringAsync().Result;
                        //Deserializing the response recieved from web api and storing into the Employee list  
                        lst = JsonConvert.DeserializeObject<List<FormulaSetupModel>>(lstResponse);

                        if (lst.Count > 0)
                        {
                            foreach (FormulaSetupModel frlmM in lst)
                            {
                                FrmlSetupMst.FormulaCode = frlmM.FormulaCode;
                                FrmlSetupMst.Description = frlmM.Description;
                                FrmlSetupMst.Formula = frlmM.Formula;
                            }
                        }
                        if (Request.IsAjaxRequest())
                        {
                            ///

                            // Get Exp Records.   
                            var allFormulaSetup = lst;
                            //
                            IEnumerable<FormulaSetupModel> filteredFormulaSetup = lst;

                            long TotalRecordsCount = lst.Count();

                            #region filters  

                            if (!string.IsNullOrEmpty(search))
                            {
                                filteredFormulaSetup = allFormulaSetup.Where(
                                u => u.FormulaCode.ToUpper().Contains(search.ToUpper())
                                || u.Description.ToUpper().Contains(search.ToUpper())
                                || u.Formula.ToUpper().Contains(search.ToUpper())
                                ).ToList();
                            }
                            #endregion
                            long FilteredRecordCount = lst.Count();

                            #region Sorting  

                            // Sorting     
                            switch (order)
                            {
                                case "0":
                                    filteredFormulaSetup = orderDir.Equals("DESC", StringComparison.CurrentCultureIgnoreCase) ? filteredFormulaSetup.OrderByDescending(p => p.FormulaCode) : filteredFormulaSetup.OrderBy(p => p.FormulaCode);
                                    break;
                                case "1":
                                    filteredFormulaSetup = orderDir.Equals("DESC", StringComparison.CurrentCultureIgnoreCase) ? filteredFormulaSetup.OrderByDescending(p => p.FormulaCode) : filteredFormulaSetup.OrderBy(p => p.FormulaCode);
                                    break;
                                case "2":
                                    filteredFormulaSetup = orderDir.Equals("DESC", StringComparison.CurrentCultureIgnoreCase) ? filteredFormulaSetup.OrderByDescending(p => p.FormulaCode) : filteredFormulaSetup.OrderBy(p => p.FormulaCode);
                                    break;
                                case "3":
                                    filteredFormulaSetup = orderDir.Equals("DESC", StringComparison.CurrentCultureIgnoreCase) ? filteredFormulaSetup.OrderByDescending(p => p.Description) : filteredFormulaSetup.OrderBy(p => p.Description);
                                    break;
                                case "4":
                                    filteredFormulaSetup = orderDir.Equals("DESC", StringComparison.CurrentCultureIgnoreCase) ? filteredFormulaSetup.OrderByDescending(p => p.Formula) : filteredFormulaSetup.OrderBy(p => p.Formula);
                                    break;

                                default:
                                    filteredFormulaSetup = filteredFormulaSetup.OrderByDescending(p => p.FormulaCode);
                                    break;
                            }
                            #endregion

                            var listCate = filteredFormulaSetup.Skip(startRec).Take(pageSize).ToList()
                                .Select(pcm => new FormulaSetupModel()
                                {
                                    FormulaCode = pcm.FormulaCode,
                                    Description = pcm.Description,
                                    Formula = pcm.Formula,

                                }).ToList();

                            if (listCate == null)
                                listCate = new List<FormulaSetupModel>();

                            return this.Json(new
                            {
                                draw = Convert.ToInt32(draw),
                                recordsTotal = TotalRecordsCount,
                                recordsFiltered = FilteredRecordCount,
                                data = listCate
                            }, JsonRequestBehavior.AllowGet);

                        }
                    }
                    else
                    {
                        return Json(AppConstant.JSONERRORLOGMSG, JsonRequestBehavior.AllowGet);
                    }
                }
            }
            catch (Exception ex)
            {
                ExceptionLogging.SendErrorToText(ex);
                ViewBag.Message = "Error occured, contact to administrator";
                return Json(AppConstant.JSONERRORLOGMSG, JsonRequestBehavior.AllowGet);
            }
            return View();
        }
        //To Add new FormulaSetup record   
        public JsonResult Add(FormulaSetupModel FRLMSTPADD)
        {
            try
            {
                FRLMSTPADD.LastModifiedBy = Session["UserName"].ToString();
                FRLMSTPADD.LoginTerminalNameIP = Session["LoginIP"].ToString() + " " + Session["LoginTerminalName"].ToString();
                using (var client = new HttpClient())
                {
                    //Passing service base url  
                    client.BaseAddress = new Uri(APIUrl);
                    var res = client.PostAsJsonAsync("FormulaSetup/InsertRecord", FRLMSTPADD).Result;
                    //Checking the response is successful or not which is sent using HttpClient  
                    if (res.IsSuccessStatusCode)
                    {
                        return Json("success", JsonRequestBehavior.AllowGet);
                    }
                    else
                    {
                        return Json(AppConstant.JSONERRORLOGMSG, JsonRequestBehavior.AllowGet);
                    }
                }

            }
            catch (Exception ex)
            {
                ExceptionLogging.SendErrorToText(ex);
                ViewBag.Message = "Error occured, contact to administrator";
                return Json(AppConstant.JSONERRORLOGMSG, JsonRequestBehavior.AllowGet);
            }
        }
        //feth FormulaSetup Master based on  code
        public JsonResult GetbyID(string ID)
        {
            try
            {
                FormulaSetupModel FRLMSTPID = new FormulaSetupModel();
                string userName = Session["UserName"].ToString();
                FRLMSTPID.FormulaCode = ID;
                FRLMSTPID.LastModifiedBy = userName;
                using (var client = new HttpClient())
                {
                    //Passing service base url  
                    client.BaseAddress = new Uri(APIUrl);
                    var res = client.PostAsJsonAsync("FormulaSetup/GetSpecRec", FRLMSTPID).Result;
                    //Checking the response is successful or not which is sent using HttpClient  
                    if (res.IsSuccessStatusCode)
                    {
                        var lstResponse = res.Content.ReadAsStringAsync().Result;
                        return Json(lstResponse, JsonRequestBehavior.AllowGet);
                    }
                    else
                    {
                        return Json(AppConstant.JSONERRORLOGMSG, JsonRequestBehavior.AllowGet);
                    }
                }
            }
            catch (Exception ex)
            {
                ExceptionLogging.SendErrorToText(ex);
                ViewBag.Message = "Error occured, contact to administrator";
                return Json(AppConstant.JSONERRORLOGMSG, JsonRequestBehavior.AllowGet);
            }

        }
        //Update FormulaSetup Master 
        public JsonResult Update(FormulaSetupModel FRLMSTPUPD)
        {
            try
            {
                FRLMSTPUPD.LastModifiedBy = Session["UserName"].ToString();
                FRLMSTPUPD.LoginTerminalNameIP = Session["LoginIP"].ToString() + " " + Session["LoginTerminalName"].ToString();
                using (var client = new HttpClient())
                {
                    //Passing service base url  
                    client.BaseAddress = new Uri(APIUrl);
                    var res = client.PostAsJsonAsync("FormulaSetup/UpdateRecord", FRLMSTPUPD).Result;
                    //Checking the response is successful or not which is sent using HttpClient  
                    if (res.IsSuccessStatusCode)
                    {
                        return Json("success", JsonRequestBehavior.AllowGet);
                    }
                    else
                    {
                        return Json(AppConstant.JSONERRORLOGMSG, JsonRequestBehavior.AllowGet);
                    }
                }
            }
            catch (Exception ex)
            {
                ExceptionLogging.SendErrorToText(ex);
                ViewBag.Message = "Error occured, contact to administrator";
                return Json(AppConstant.JSONERRORLOGMSG, JsonRequestBehavior.AllowGet);
            }

        }
        //Delete FormulaSetup Master
        public JsonResult Delete(string ID)
        {
            try
            {
                FormulaSetupModel FRLMSTDEL = new FormulaSetupModel();
                FRLMSTDEL.FormulaCode = ID;
                FRLMSTDEL.LastModifiedBy = Session["UserName"].ToString();
                FRLMSTDEL.LoginTerminalNameIP = Session["LoginIP"].ToString() + " " + Session["LoginTerminalName"].ToString();
                using (var client = new HttpClient())
                {
                    //Passing service base url  
                    client.BaseAddress = new Uri(APIUrl);
                    var res = client.PostAsJsonAsync("FormulaSetup/DeleteRecord", FRLMSTDEL).Result;
                    //Checking the response is successful or not which is sent using HttpClient  
                    if (res.IsSuccessStatusCode)
                    {
                        return Json("success", JsonRequestBehavior.AllowGet);
                    }
                    else
                    {
                        return Json(AppConstant.JSONERRORLOGMSG, JsonRequestBehavior.AllowGet);
                    }
                }
            }
            catch (Exception ex)
            {
                ExceptionLogging.SendErrorToText(ex);
                ViewBag.Message = "Error occured, contact to administrator";
                return Json(AppConstant.JSONERRORLOGMSG, JsonRequestBehavior.AllowGet);
            }

        }
    }
}