﻿using Payroll.Models;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Text;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;
using System.Web.Mvc.Html;

namespace Payroll.Controllers
{
    public class LocationController : Controller
    {
        // GET: Location
        string APIUrl = ConfigurationManager.AppSettings["APIURL"].ToString();
        public ActionResult Location()
        {
            try
            {
                return View();
            }
            catch (Exception ex)
            {
                ExceptionLogging.SendErrorToText(ex);
                ViewBag.Message = "Error occured, contact to administrator";
                return View();
            }
        }
        public JsonResult List()
        {
            try
            {
                LocationModel LocationMod = new LocationModel();
                List<LocationModel> lst = new List<LocationModel>();
                LocationMod.LastModifiedBy = Session["UserName"].ToString();
                LocationMod.LoginTerminalNameIP = Session["LoginIP"].ToString() + " " + Session["LoginTerminalName"].ToString();

                using (var client = new HttpClient())
                {
                    client.BaseAddress = new Uri(APIUrl);
                    var res = client.PostAsJsonAsync("Location/GetAllList", LocationMod).Result;
                    if (res.IsSuccessStatusCode)
                    {
                        var lstResponse = res.Content.ReadAsStringAsync().Result;
                        lst = JsonConvert.DeserializeObject<List<LocationModel>>(lstResponse);
                    }
                }
                return Json(lst, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                ExceptionLogging.SendErrorToText(ex);
                return Json(AppConstant.JSONERRORLOGMSG, JsonRequestBehavior.AllowGet);
            }

        }
        //Get all Location Master for jquery data table
        [HttpPost]
        public ActionResult GetAllLocation()
        {
            LocationModel LocationMst = new LocationModel();
            List<LocationModel> lst = new List<LocationModel>();
            try
            {
                string userName = Session["UserName"].ToString();
                LocationMst.LastModifiedBy = userName;
                string draw = Request.Form.GetValues("draw")[0];
                string order = Request.Form.GetValues("order[0][column]")[0];
                string orderDir = Request.Form.GetValues("order[0][dir]")[0];
                int startRec = Convert.ToInt32(Request.Form.GetValues("start")[0]);
                int pageSize = Convert.ToInt32(Request.Form.GetValues("length")[0]);
                string search = Request.Form.GetValues("search[value]").FirstOrDefault();

                ///

                ///
                using (var client = new HttpClient())
                {

                    client.BaseAddress = new Uri(APIUrl);

                    var res = client.PostAsJsonAsync("Location/GetAllList", LocationMst).Result;

                    //Checking the response is successful or not which is sent using HttpClient  
                    if (res.IsSuccessStatusCode)
                    {

                        var lstResponse = res.Content.ReadAsStringAsync().Result;
                        //Deserializing the response recieved from web api and storing into the Employee list  
                        lst = JsonConvert.DeserializeObject<List<LocationModel>>(lstResponse);

                        if (lst.Count > 0)
                        {
                            foreach (LocationModel LocationM in lst)
                            {
                                LocationMst.LocationCode = LocationM.LocationCode;
                                LocationMst.LocationName = LocationM.LocationName;
                                LocationMst.LocationCity = LocationM.LocationCity;
                                LocationMst.LocationState = LocationM.LocationState;
                                LocationMst.LocationPostalCode = LocationM.LocationPostalCode;
                                LocationMst.LocationCountry = LocationM.LocationCountry;
                                LocationMst.LocationAddress1 = LocationM.LocationAddress1;
                                LocationMst.LocationAddress2 = LocationM.LocationAddress2;
                                LocationMst.LocationContactEmailID = LocationM.LocationContactEmailID;
                            }
                        }
                        if (Request.IsAjaxRequest())
                        {
                            // Get Exp Records.   
                            var allLocation = lst;
                            //
                            IEnumerable<LocationModel> filteredLocation = lst;

                            long TotalRecordsCount = lst.Count();

                            #region filters  

                            if (!string.IsNullOrEmpty(search))
                            {
                                filteredLocation = allLocation.Where(
                                u => u.LocationCode.ToUpper().Contains(search.ToUpper())
                                || u.LocationName.ToUpper().Contains(search.ToUpper())
                                || u.LocationCity.ToUpper().Contains(search.ToUpper())
                                || u.LocationState.ToUpper().Contains(search.ToUpper())
                                || u.LocationPostalCode.ToUpper().Contains(search.ToUpper())
                                || u.LocationCountry.ToUpper().Contains(search.ToUpper())
                                || u.LocationAddress1.ToUpper().Contains(search.ToUpper())
                                || u.LocationAddress2.ToUpper().Contains(search.ToUpper())
                                || u.LocationContactEmailID.ToUpper().Contains(search.ToUpper())
                                ).ToList();
                            }
                            #endregion
                            long FilteredRecordCount = lst.Count();

                            #region Sorting  

                            // Sorting     
                            switch (order)
                            {
                                case "0":
                                    filteredLocation = orderDir.Equals("DESC", StringComparison.CurrentCultureIgnoreCase) ? filteredLocation.OrderByDescending(p => p.LocationCode) : filteredLocation.OrderBy(p => p.LocationCode);
                                    break;
                                case "1":
                                    filteredLocation = orderDir.Equals("DESC", StringComparison.CurrentCultureIgnoreCase) ? filteredLocation.OrderByDescending(p => p.LocationCode) : filteredLocation.OrderBy(p => p.LocationCode);
                                    break;
                                case "2":
                                    filteredLocation = orderDir.Equals("DESC", StringComparison.CurrentCultureIgnoreCase) ? filteredLocation.OrderByDescending(p => p.LocationCode) : filteredLocation.OrderBy(p => p.LocationCode);
                                    break;
                                case "3":
                                    filteredLocation = orderDir.Equals("DESC", StringComparison.CurrentCultureIgnoreCase) ? filteredLocation.OrderByDescending(p => p.LocationName) : filteredLocation.OrderBy(p => p.LocationName);
                                    break;
                                case "4":
                                    filteredLocation = orderDir.Equals("DESC", StringComparison.CurrentCultureIgnoreCase) ? filteredLocation.OrderByDescending(p => p.LocationCity) : filteredLocation.OrderBy(p => p.LocationCity);
                                    break;
                                case "5":
                                    filteredLocation = orderDir.Equals("DESC", StringComparison.CurrentCultureIgnoreCase) ? filteredLocation.OrderByDescending(p => p.LocationState) : filteredLocation.OrderBy(p => p.LocationState);
                                    break;
                                case "6":
                                    filteredLocation = orderDir.Equals("DESC", StringComparison.CurrentCultureIgnoreCase) ? filteredLocation.OrderByDescending(p => p.LocationPostalCode) : filteredLocation.OrderBy(p => p.LocationPostalCode);
                                    break;
                                case "7":
                                    filteredLocation = orderDir.Equals("DESC", StringComparison.CurrentCultureIgnoreCase) ? filteredLocation.OrderByDescending(p => p.LocationCountry) : filteredLocation.OrderBy(p => p.LocationCountry);
                                    break;
                                case "8":
                                    filteredLocation = orderDir.Equals("DESC", StringComparison.CurrentCultureIgnoreCase) ? filteredLocation.OrderByDescending(p => p.LocationAddress1) : filteredLocation.OrderBy(p => p.LocationAddress1);
                                    break;
                                case "9":
                                    filteredLocation = orderDir.Equals("DESC", StringComparison.CurrentCultureIgnoreCase) ? filteredLocation.OrderByDescending(p => p.LocationAddress2) : filteredLocation.OrderBy(p => p.LocationAddress2);
                                    break;
                                case "10":
                                    filteredLocation = orderDir.Equals("DESC", StringComparison.CurrentCultureIgnoreCase) ? filteredLocation.OrderByDescending(p => p.LocationContactEmailID) : filteredLocation.OrderBy(p => p.LocationContactEmailID);
                                    break;
                                default:
                                    filteredLocation = filteredLocation.OrderByDescending(p => p.LocationCode);
                                    break;
                            }
                            #endregion

                            var listLocation = filteredLocation.Skip(startRec).Take(pageSize).ToList()
                                .Select(pcm => new LocationModel()
                                {
                                    LocationCode = pcm.LocationCode,
                                    LocationName = pcm.LocationName,
                                    LocationCity = pcm.LocationCity,
                                    LocationState = pcm.LocationState,
                                    LocationPostalCode = pcm.LocationPostalCode,
                                    LocationCountry = pcm.LocationCountry,
                                    LocationAddress1 = pcm.LocationAddress1,
                                    LocationAddress2 = pcm.LocationAddress2,
                                    LocationContactEmailID = pcm.LocationContactEmailID,

                                }).ToList();

                            if (listLocation == null)
                                listLocation = new List<LocationModel>();

                            return this.Json(new
                            {
                                draw = Convert.ToInt32(draw),
                                recordsTotal = TotalRecordsCount,
                                recordsFiltered = FilteredRecordCount,
                                data = listLocation
                            }, JsonRequestBehavior.AllowGet);

                        }
                    }
                    else
                    {
                        return Json(AppConstant.JSONERRORLOGMSG, JsonRequestBehavior.AllowGet);
                    }
                }
            }
            catch (Exception ex)
            {
                ExceptionLogging.SendErrorToText(ex);
                ViewBag.Message = "Error occured, contact to administrator";
                return Json(AppConstant.JSONERRORLOGMSG, JsonRequestBehavior.AllowGet);
            }
            return View();
        }
        //To Add new Location record   
        public JsonResult Add(LocationModel LOCADD)
        {
            try
            {
                LOCADD.LastModifiedBy = Session["UserName"].ToString();
                LOCADD.LoginTerminalNameIP = Session["LoginIP"].ToString() + " " + Session["LoginTerminalName"].ToString();
                using (var client = new HttpClient())
                {
                    //Passing service base url  
                    client.BaseAddress = new Uri(APIUrl);
                    var res = client.PostAsJsonAsync("Location/InsertRecord", LOCADD).Result;
                    //Checking the response is successful or not which is sent using HttpClient  
                    if (res.IsSuccessStatusCode)
                    {
                        return Json("success", JsonRequestBehavior.AllowGet);
                    }
                    else
                    {
                        return Json(AppConstant.JSONERRORLOGMSG, JsonRequestBehavior.AllowGet);
                    }
                }

            }
            catch (Exception ex)
            {
                ExceptionLogging.SendErrorToText(ex);
                ViewBag.Message = "Error occured, contact to administrator";
                return Json(AppConstant.JSONERRORLOGMSG, JsonRequestBehavior.AllowGet);
            }
        }
        //feth Location Master based on  code
        public JsonResult GetbyID(string ID)
        {
            try
            {
                LocationModel LOCID = new LocationModel();
                string userName = Session["UserName"].ToString();
                LOCID.LocationCode = ID;
                LOCID.LastModifiedBy = userName;
                using (var client = new HttpClient())
                {
                    //Passing service base url  
                    client.BaseAddress = new Uri(APIUrl);
                    var res = client.PostAsJsonAsync("Location/GetSpecRec", LOCID).Result;
                    //Checking the response is successful or not which is sent using HttpClient  
                    if (res.IsSuccessStatusCode)
                    {
                        var lstResponse = res.Content.ReadAsStringAsync().Result;
                        return Json(lstResponse, JsonRequestBehavior.AllowGet);
                    }
                    else
                    {
                        return Json(AppConstant.JSONERRORLOGMSG, JsonRequestBehavior.AllowGet);
                    }
                }
            }
            catch (Exception ex)
            {
                ExceptionLogging.SendErrorToText(ex);
                ViewBag.Message = "Error occured, contact to administrator";
                return Json(AppConstant.JSONERRORLOGMSG, JsonRequestBehavior.AllowGet);
            }
        }
        //Update Location Master 
        public JsonResult Update(LocationModel LOCUPD)
        {
            try
            {
                LOCUPD.LastModifiedBy = Session["UserName"].ToString();
                LOCUPD.LoginTerminalNameIP = Session["LoginIP"].ToString() + " " + Session["LoginTerminalName"].ToString();
                using (var client = new HttpClient())
                {
                    //Passing service base url  
                    client.BaseAddress = new Uri(APIUrl);
                    var res = client.PostAsJsonAsync("Location/UpdateRecord", LOCUPD).Result;
                    //Checking the response is successful or not which is sent using HttpClient  
                    if (res.IsSuccessStatusCode)
                    {
                        return Json("success", JsonRequestBehavior.AllowGet);
                    }
                    else
                    {
                        return Json(AppConstant.JSONERRORLOGMSG, JsonRequestBehavior.AllowGet);
                    }
                }
            }
            catch (Exception ex)
            {
                ExceptionLogging.SendErrorToText(ex);
                ViewBag.Message = "Error occured, contact to administrator";
                return Json(AppConstant.JSONERRORLOGMSG, JsonRequestBehavior.AllowGet);
            }

        }
        //Delete Location Master
        public JsonResult Delete(string ID)
        {
            try
            {
                LocationModel LOCEDEL = new LocationModel();
                LOCEDEL.LocationCode = ID;
                LOCEDEL.LastModifiedBy = Session["UserName"].ToString();
                LOCEDEL.LoginTerminalNameIP = Session["LoginIP"].ToString() + " " + Session["LoginTerminalName"].ToString();
                using (var client = new HttpClient())
                {
                    //Passing service base url  
                    client.BaseAddress = new Uri(APIUrl);
                    var res = client.PostAsJsonAsync("Location/DeleteRecord", LOCEDEL).Result;
                    //Checking the response is successful or not which is sent using HttpClient  
                    if (res.IsSuccessStatusCode)
                    {
                        return Json("success", JsonRequestBehavior.AllowGet);
                    }
                    else
                    {
                        return Json(AppConstant.JSONERRORLOGMSG, JsonRequestBehavior.AllowGet);
                    }
                }
            }
            catch (Exception ex)
            {
                ExceptionLogging.SendErrorToText(ex);
                ViewBag.Message = "Error occured, contact to administrator";
                return Json(AppConstant.JSONERRORLOGMSG, JsonRequestBehavior.AllowGet);
            }

        }

        //feth Location Master based on  code
        public JsonResult GetbyLoccode(string ID)
        {
            try
            {
                LocationModel LOCID = new LocationModel();
                string userName = Session["UserName"].ToString();
                LOCID.LocationCode = ID;
                LOCID.LastModifiedBy = userName;
                using (var client = new HttpClient())
                {
                    //Passing service base url  
                    client.BaseAddress = new Uri(APIUrl);
                    var res = client.PostAsJsonAsync("Location/GetLoccode", LOCID).Result;
                    //Checking the response is successful or not which is sent using HttpClient  
                    if (res.IsSuccessStatusCode)
                    {
                        var lstResponse = res.Content.ReadAsStringAsync().Result;
                        return Json(lstResponse, JsonRequestBehavior.AllowGet);
                    }
                    else
                    {
                        return Json(AppConstant.JSONERRORLOGMSG, JsonRequestBehavior.AllowGet);
                    }
                }
            }
            catch (Exception ex)
            {
                ExceptionLogging.SendErrorToText(ex);
                ViewBag.Message = "Error occured, contact to administrator";
                return Json(AppConstant.JSONERRORLOGMSG, JsonRequestBehavior.AllowGet);
            }
        }
    }
}