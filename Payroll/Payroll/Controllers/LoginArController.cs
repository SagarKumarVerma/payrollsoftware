﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace Payroll.Controllers
{
    public class LoginArController : Controller
    {
        // GET: LoginAr
        public ActionResult LoginAr()
        {
            return View();
        }
        [HttpPost]
        public ActionResult Lang(string ddlLanguage)
        {
            Session["LangType"] = ddlLanguage.Trim();
            return Json("LoginAr", JsonRequestBehavior.AllowGet);
        }
        [HttpPost]
        public ActionResult LangDrp(string ddlLanguage)
        {
            ViewBag.Lang = ddlLanguage.Trim();
            return Json("ddlLanguage", JsonRequestBehavior.AllowGet);
        }
    }
}