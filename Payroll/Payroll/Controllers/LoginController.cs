﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace Payroll.Controllers
{
    public class LoginController : Controller
    {
        // GET: Login
        [HttpGet]
        public ActionResult Login()
        {
            return View();
        }
        [HttpPost]
        public ActionResult Lang(string ddlLanguage)
        {
            Session["LangType"] = ddlLanguage.Trim();
            return Json("Login",JsonRequestBehavior.AllowGet);
        }
        [HttpPost]
        public ActionResult LangDrp(string ddlLanguage)
        {
            ViewBag.Lang = ddlLanguage.Trim();
            return Json("ddlLanguage", JsonRequestBehavior.AllowGet);
        }
    }
}