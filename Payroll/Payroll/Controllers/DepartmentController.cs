﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Payroll.Models;
using System.Configuration;
using Newtonsoft.Json;
using System.Net.Http;
using System.Net.Http.Headers;


namespace Payroll.Controllers
{
    public class DepartmentController : Controller
    {
        string APIUrl = ConfigurationManager.AppSettings["APIURL"].ToString();
        // GET: Department
        public ActionResult Department()
        {
            try
            {
                return View();
            }
            catch (Exception ex)
            {
                ex.ToString();
                ViewBag.Message = "Error occured, contact to administrator";
                return View();
            }
        }
        public JsonResult List()
        {
            DepartmentModel DeptMast = new DepartmentModel();
            List<DepartmentModel> lst = new List<DepartmentModel>();
            try
            {
                using (var client = new HttpClient())
                {
                    client.BaseAddress = new Uri(APIUrl);

                    var res = client.PostAsJsonAsync("Department/GetAllDepartment", DeptMast).Result;
                    if (res.IsSuccessStatusCode)
                    {
                        var lstResponse = res.Content.ReadAsStringAsync().Result;
                        lst = JsonConvert.DeserializeObject<List<DepartmentModel>>(lstResponse);
                    }
                }
                return Json(lst, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                ex.ToString();
                ViewBag.Message = "Error occured, contact to administrator";
                return Json("error", JsonRequestBehavior.AllowGet);
            }
        }     
        [HttpPost]
        public ActionResult GetAllDepartment()
        {
            DepartmentModel DeptMast = new DepartmentModel();
            List<DepartmentModel> lst = new List<DepartmentModel>();
            try
            {
                string draw = Request.Form.GetValues("draw")[0];
                string order = Request.Form.GetValues("order[0][column]")[0];
                string orderDir = Request.Form.GetValues("order[0][dir]")[0];
                int startRec = Convert.ToInt32(Request.Form.GetValues("start")[0]);
                int pageSize = Convert.ToInt32(Request.Form.GetValues("length")[0]);
                string search = Request.Form.GetValues("search[value]").FirstOrDefault();

                using (var client = new HttpClient())
                {
                    client.BaseAddress = new Uri(APIUrl);
                    var res = client.PostAsJsonAsync("Department/GetAllDepartment", DeptMast).Result;

                    if (res.IsSuccessStatusCode)
                    {
                        var lstResponse = res.Content.ReadAsStringAsync().Result;
                        lst = JsonConvert.DeserializeObject<List<DepartmentModel>>(lstResponse);

                        if (lst.Count > 0)
                        {
                            foreach (DepartmentModel DeptMstLst in lst)
                            {
                                DeptMast.DepartmentCode = DeptMstLst.DepartmentCode;
                                DeptMast.DepartmentName = DeptMstLst.DepartmentName;
                                DeptMast.IsActive = DeptMstLst.IsActive;
                            }
                        }

                        //==
                        if (Request.IsAjaxRequest())
                        {
                            // Get Department Master Records. 
                            var allDept = lst; //langDB.GetAllLanguage();
                            IEnumerable<DepartmentModel> filteredLang = lst; //langDB.GetAllDepartment();
                            long TotalRecordsCount = lst.Count(); //langDB.GetAllDepartment().Count();
                            #region filters  
                            if (!string.IsNullOrEmpty(search))
                            {
                                filteredLang = allDept.Where(
                                u => u.IsActive.ToUpper().Contains(search.ToUpper())
                                || u.DepartmentCode.ToUpper().Contains(search.ToUpper())
                                || u.DepartmentName.ToUpper().Contains(search.ToUpper())
                                || u.IsActive.ToUpper().Contains(search.ToUpper())
                                ).ToList();
                            }
                            #endregion
                            long FilteredRecordCount = lst.Count(); //langDB.GetAllDepartment().Count();
                            #region Sorting  
                            // Sorting     
                            switch (order)
                            {
                                case "0":
                                    filteredLang = orderDir.Equals("DESC", StringComparison.CurrentCultureIgnoreCase) ? filteredLang.OrderByDescending(p => p.DepartmentCode) : filteredLang.OrderBy(p => p.DepartmentCode);
                                    break;
                                case "1":
                                    filteredLang = orderDir.Equals("DESC", StringComparison.CurrentCultureIgnoreCase) ? filteredLang.OrderByDescending(p => p.DepartmentCode) : filteredLang.OrderBy(p => p.DepartmentCode);
                                    break;
                                case "2":
                                    filteredLang = orderDir.Equals("DESC", StringComparison.CurrentCultureIgnoreCase) ? filteredLang.OrderByDescending(p => p.DepartmentCode) : filteredLang.OrderBy(p => p.DepartmentCode);
                                    break;
                                case "3":
                                    filteredLang = orderDir.Equals("DESC", StringComparison.CurrentCultureIgnoreCase) ? filteredLang.OrderByDescending(p => p.IsActive) : filteredLang.OrderBy(p => p.IsActive);
                                    break;
                                case "4":
                                    filteredLang = orderDir.Equals("DESC", StringComparison.CurrentCultureIgnoreCase) ? filteredLang.OrderByDescending(p => p.DepartmentName) : filteredLang.OrderBy(p => p.DepartmentName);
                                    break;
                                default:
                                    filteredLang = filteredLang.OrderByDescending(p => p.DepartmentCode);
                                    break;
                            }
                            #endregion

                            var listLang = filteredLang.Skip(startRec).Take(pageSize).ToList()
                                .Select(d => new DepartmentModel()
                                {

                                    IsActive = d.IsActive,
                                    DepartmentCode = d.DepartmentCode,
                                    DepartmentName = d.DepartmentName
                                }).ToList();

                            if (listLang == null)
                                listLang = new List<DepartmentModel>();

                            return this.Json(new
                            {
                                draw = Convert.ToInt32(draw),
                                recordsTotal = TotalRecordsCount,
                                recordsFiltered = FilteredRecordCount,
                                data = listLang
                            }, JsonRequestBehavior.AllowGet);

                        }
                        //==
                    }
                }
            }
            catch (Exception ex)
            {
                ex.ToString();
                ViewBag.Message = "Error occured, contact to administrator";
                return Json("error", JsonRequestBehavior.AllowGet);
            }
            return View();
        }
        // Get all Department data
        //public JsonResult GetAutoGenerateCode()
        //{
        //    try
        //    {
        //        DepartmentModel DeptMastMod = new DepartmentModel();
        //        DepartmentModel objDepartmentModel = new DepartmentModel();
        //        List<DepartmentModel> lst = new List<DepartmentModel>();

        //        using (var client = new HttpClient())
        //        {
        //            client.BaseAddress = new Uri(APIUrl);

        //            var res = client.PostAsJsonAsync("Department/GetAllDepartment", DeptMastMod).Result;
        //            if (res.IsSuccessStatusCode)
        //            {
        //                var lstResponse = res.Content.ReadAsStringAsync().Result;
        //                lst = JsonConvert.DeserializeObject<List<DepartmentModel>>(lstResponse);
        //            }
        //        }
        //        var lastcode = lst.ToList().OrderByDescending(m => m.DepartmentCode).FirstOrDefault();
        //        if (lastcode == null)
        //        {
        //            objDepartmentModel.DepartmentCode = "00001";
        //        }
        //        else
        //        {
        //            objDepartmentModel.DepartmentCode = (Convert.ToInt32(lastcode.DepartmentCode) + 1).ToString("D5").PadLeft(0);
        //        }
        //        return Json(objDepartmentModel.DepartmentCode, JsonRequestBehavior.AllowGet);
        //    }
        //    catch (Exception ex)
        //    {
        //        ex.ToString();
        //        ViewBag.Message = "Error occured, contact to administrator";
        //        return Json("error", JsonRequestBehavior.AllowGet);
        //    }
        //}
        public JsonResult ListAllDepartment()
        {
            DepartmentModel DeptMast = new DepartmentModel();
            List<DepartmentModel> lst = new List<DepartmentModel>();
            try
            {
                using (var client = new HttpClient())
                {
                    client.BaseAddress = new Uri(APIUrl);

                    var res = client.PostAsJsonAsync("Department/GetAllDepartment", DeptMast).Result;
                    if (res.IsSuccessStatusCode)
                    {
                        var lstResponse = res.Content.ReadAsStringAsync().Result;
                        lst = JsonConvert.DeserializeObject<List<DepartmentModel>>(lstResponse);
                    }
                }
                return Json(lst, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                ex.ToString();
                ViewBag.Message = "Error occured, contact to administrator";
                return Json("error", JsonRequestBehavior.AllowGet);
            }
        }
        public JsonResult Add(DepartmentModel DeptMast)
        {
            try
            {
                DeptMast.LastModifiedBy = Session["UserName"].ToString();
                DeptMast.LoginTerminalNameIP = Session["LoginIP"].ToString() + " " + Session["LoginTerminalName"].ToString();

                using (var client = new HttpClient())
                {
                    //Passing service base url  
                    client.BaseAddress = new Uri(APIUrl);

                    var res = client.PostAsJsonAsync("Department/AddDepartment", DeptMast).Result;

                    //Checking the response is successful or not which is sent using HttpClient  
                    if (res.IsSuccessStatusCode)
                    {
                        return Json("success", JsonRequestBehavior.AllowGet);
                    }
                    else
                    {
                        return Json("error", JsonRequestBehavior.AllowGet);
                    }
                }
            }
            catch (Exception ex)
            {
                ex.ToString();
                ViewBag.Message = "Error occured, contact to administrator";
                return Json("error", JsonRequestBehavior.AllowGet);
            }
        }
        public JsonResult GetbyID(string ID)
        {
            try
            {
                DepartmentModel DeptMast = new DepartmentModel();
                string userName = Session["UserName"].ToString();
                DeptMast.DepartmentCode = ID;
                DeptMast.LastModifiedBy = userName;
                using (var client = new HttpClient())
                {
                    //Passing service base url  
                    client.BaseAddress = new Uri(APIUrl);

                    var res = client.PostAsJsonAsync("Department/GetSpecRec", DeptMast).Result;

                    //Checking the response is successful or not which is sent using HttpClient  
                    if (res.IsSuccessStatusCode)
                    {
                        var lstResponse = res.Content.ReadAsStringAsync().Result;

                        return Json(lstResponse, JsonRequestBehavior.AllowGet);
                    }
                    else
                    {
                        return Json(AppConstant.JSONERRORLOGMSG, JsonRequestBehavior.AllowGet);
                    }
                }
            }
            catch (Exception ex)
            {
                ExceptionLogging.SendErrorToText(ex);
                ViewBag.Message = "Error occured, contact to administrator";
                return Json(AppConstant.JSONERRORLOGMSG, JsonRequestBehavior.AllowGet);
            }

        }
        //public JsonResult GetbyID(string DepartmentCode)
        //{
        //    try
        //    {
        //        DepartmentModel DeptMast = new DepartmentModel();
        //        List<DepartmentModel> lst = new List<DepartmentModel>();

        //        using (var client = new HttpClient())
        //        {
        //            client.BaseAddress = new Uri(APIUrl);

        //            var res = client.PostAsJsonAsync("Department/GetAllDepartment", DeptMast).Result;
        //            if (res.IsSuccessStatusCode)
        //            {
        //                var lstResponse = res.Content.ReadAsStringAsync().Result;
        //                lst = JsonConvert.DeserializeObject<List<DepartmentModel>>(lstResponse);
        //            }
        //        }
        //        var DeptCode = lst.Find(x => x.DepartmentCode.Equals(DepartmentCode.ToString()));
        //        return Json(DeptCode, JsonRequestBehavior.AllowGet);
        //    }
        //    catch (Exception ex)
        //    {
        //        ex.ToString();
        //        ViewBag.Message = "Error occured, contact to administrator";
        //        return Json("error", JsonRequestBehavior.AllowGet);
        //    }
        //}
        public JsonResult Update(DepartmentModel DeptMast)
        {
            try
            {
                DeptMast.LastModifiedBy = Session["UserName"].ToString();
                DeptMast.LoginTerminalNameIP = Session["LoginIP"].ToString() + " " + Session["LoginTerminalName"].ToString();
                using (var client = new HttpClient())
                {
                    //Passing service base url  
                    client.BaseAddress = new Uri(APIUrl);

                    var res = client.PostAsJsonAsync("Department/UpdateDepartment", DeptMast).Result;

                    //Checking the response is successful or not which is sent using HttpClient  
                    if (res.IsSuccessStatusCode)
                    {
                        return Json("success", JsonRequestBehavior.AllowGet);
                    }
                    else
                    {
                        return Json("error", JsonRequestBehavior.AllowGet);
                    }
                }
            }
            catch (Exception ex)
            {
                ex.ToString();
                ViewBag.Message = "Error occured, contact to administrator";
                return Json("error", JsonRequestBehavior.AllowGet);
            }

        }   
        public JsonResult Delete(string ID)
        {
            try
            {
                DepartmentModel DeptMast = new DepartmentModel();
                DeptMast.DepartmentCode = ID;
                DeptMast.LastModifiedBy = Session["UserName"].ToString();
                DeptMast.LoginTerminalNameIP = Session["LoginIP"].ToString() + " " + Session["LoginTerminalName"].ToString();
                using (var client = new HttpClient())
                {
                    //Passing service base url  
                    client.BaseAddress = new Uri(APIUrl);

                    var res = client.PostAsJsonAsync("Department/DeleteDepartment", DeptMast).Result;

                    //Checking the response is successful or not which is sent using HttpClient  
                    if (res.IsSuccessStatusCode)
                    {
                        return Json("success", JsonRequestBehavior.AllowGet);
                    }
                    else
                    {
                        return Json("error", JsonRequestBehavior.AllowGet);
                    }
                }
            }
            catch (Exception ex)
            {
                ex.ToString();
                ViewBag.Message = "Error occured, contact to administrator";
                return Json("error", JsonRequestBehavior.AllowGet);
            }
        }
        //feth Department Master based on  code
        public JsonResult GetbyDeptCode(string ID)
        {
            try
            {
                DepartmentModel DeptMast = new DepartmentModel();
                string userName = Session["UserName"].ToString();
                DeptMast.DepartmentCode = ID;
                DeptMast.LastModifiedBy = userName;
                using (var client = new HttpClient())
                {
                    //Passing service base url  
                    client.BaseAddress = new Uri(APIUrl);

                    var res = client.PostAsJsonAsync("Department/GetDeptcode", DeptMast).Result;

                    //Checking the response is successful or not which is sent using HttpClient  
                    if (res.IsSuccessStatusCode)
                    {
                        var lstResponse = res.Content.ReadAsStringAsync().Result;

                        return Json(lstResponse, JsonRequestBehavior.AllowGet);
                    }
                    else
                    {
                        return Json(AppConstant.JSONERRORLOGMSG, JsonRequestBehavior.AllowGet);
                    }
                }
            }
            catch (Exception ex)
            {
                ExceptionLogging.SendErrorToText(ex);
                ViewBag.Message = "Error occured, contact to administrator";
                return Json(AppConstant.JSONERRORLOGMSG, JsonRequestBehavior.AllowGet);
            }

        }
    }
}