CREATE TABLE [dbo].[tblLocation](
	[LocationCode] [char](5) NOT NULL,
	[LocationName] [nvarchar](100) NULL,
	[City] [nvarchar] (100) NULL,
	[State] [nvarchar](100) NULL,
	[PostalCode] [nvarchar](10) NULL,
	[Country] [nvarchar](50) NOT NULL,
	[Address1] [nvarchar](250) NULL,
	[Address2] [nvarchar] (250) NULL,
	[ContactEmailID] [nvarchar](50) NULL,
	[CreatedDate] [DATETIME] NULL,
    [LastModifiedDate] [DATETIME]  NULL,
	[LastModifiedBy] varchar(50),
 CONSTRAINT [PK_tblLocation] PRIMARY KEY CLUSTERED 
(
	[LocationCode] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

go
Drop procedure Payroll_TblLocationMaster_Add
go
Create procedure Payroll_TblLocationMaster_Add     
(   
    @LocationCode char(5),
    @LocationName nvarchar(100),        
    @City nvarchar(100),  
	@State   nvarchar(100),  
	@PostalCode   nvarchar(10),  
	@Country nvarchar(50),        
    @Address1 nvarchar(250),  
	@Address2   nvarchar(250),  
	@ContactEmailID   nvarchar(50), 
    @LastModifiedBy Varchar(20)   
)    
as     
Begin     
    Insert into tblLocation (LocationCode,LocationName,City,[State],PostalCode,Country,Address1,Address2,ContactEmailID,CreatedDate,LastModifiedBy,LastModifiedDate)     
    Values (@LocationCode,@LocationName,@City,@State,@PostalCode,@Country,@Address1,@Address2,@ContactEmailID,GETDATE(),@LastModifiedBy,GETDATE())     
End

go
Drop procedure Payroll_TblLocationMaster_Upd
go
Create procedure Payroll_TblLocationMaster_Upd      
(      
    @LocationCode char(5),
    @LocationName nvarchar(100),        
    @City nvarchar(100),  
	@State   nvarchar(100),  
	@PostalCode   nvarchar(10),  
	@Country nvarchar(50),        
    @Address1 nvarchar(250),  
	@Address2   nvarchar(250),  
	@ContactEmailID   nvarchar(50), 
    @LastModifiedBy Varchar(20)  
)      
as      
begin      
   Update tblLocation       
   set LocationName=@LocationName,          
   City=@City, 
   [State]=@State, 
   PostalCode=@PostalCode,
   Country=@Country, 
   Address1=@Address1, 
   Address2=@Address2, 
   ContactEmailID=@ContactEmailID,     
   LastModifiedBy=@LastModifiedBy,  
   LastModifiedDate=GETDATE()   
   where LocationCode=@LocationCode      
End
go
Drop procedure Payroll_TblLocationMaster_Del
go
Create procedure Payroll_TblLocationMaster_Del     
(      
    @LocationCode char(5)    
)      
as       
begin      
   Delete from tblLocation where LocationCode =@LocationCode      
End
go
Drop procedure Payroll_TblLocationMaster_AllRec
go
Create procedure Payroll_TblLocationMaster_AllRec   
as    
Begin    
select LocationCode,LocationName,City,[State],PostalCode,Country,Address1,Address2,ContactEmailID,CreatedDate ,LastModifiedBy,LastModifiedDate from tblLocation     
End
go
Drop procedure Payroll_TblLocationMaster_GetLocationData 
go
Create procedure Payroll_TblLocationMaster_GetLocationData 
(
 @LocationCode char(5)   
)  
as    
Begin    
   SELECT * FROM tblLocation where LocationCode= @LocationCode   
End

GO
Drop procedure Payroll_TblLocationMaster_LocationNum 
go
Create procedure Payroll_TblLocationMaster_LocationNum 
(
 @LocationCode char(5)    
)  
as    
Begin    
     SELECT top 1 LocationCode FROM tblLocation order by LocationCode desc
End
GO
--CheckIsLocationCode Payroll Location Master
drop procedure Payroll_TblLocationMaster_CheckIsCode
GO
CREATE procedure Payroll_TblLocationMaster_CheckIsCode
(
@LocationCode varchar(5)
)  
as      
begin
SELECT count(LocationCode) cnt  FROM tblLocation where LocationCode=@LocationCode 
end
GO
