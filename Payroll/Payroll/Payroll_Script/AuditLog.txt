-- Payroll Audit Log Table
Create table Payroll_TblAuditLog (
	UserName	 	varchar(20),
	LogDate		 	datetime,
	UserData		varchar(50),
	ModuleName	 	varchar(50),
	IdFieldName		varchar(50),
	IdFieldValue		varchar(50),
	ChangeRemark		varchar(50),
	LogRemark	 	varchar(2000))
GO
Create Index IDX_Payroll_TblAuditLog_UserName on Payroll_TblAuditLog(UserName)
GO
Create Index IDX_Payroll_TblAuditLog_LogDate on Payroll_TblAuditLog(LogDate)
GO
Create Index IDX_Payroll_TblAuditLog_IdFieldName on Payroll_TblAuditLog(IdFieldName)
GO
drop procedure SP_AuditLogTrail
GO
CREATE procedure SP_AuditLogTrail
(
@UserName varchar(20),
@UserData varchar(50), 
@ModuleName varchar(50),
@IdFieldName varchar(50),
@IdFieldValue varchar(50),
@ChangeRemark varchar(50),
@LogRemark varchar(2000)
)
as      
begin
insert into Payroll_TblAuditLog(UserName, LogDate, UserData, ModuleName, IdFieldName, IdFieldValue, ChangeRemark, LogRemark)
values
(@UserName, getdate(), @UserData, @ModuleName, @IdFieldName, @IdFieldValue, @ChangeRemark, @LogRemark)
end