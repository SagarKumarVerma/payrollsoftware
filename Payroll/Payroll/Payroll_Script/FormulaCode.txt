Create table Pay_FormulaCode
( 
Id bigint IDENTITY(1,1) NOT NULL,
Code char (1),
Flag char(1)
)
go
Insert into Pay_FormulaCode values ('A','Y')
go
Insert into Pay_FormulaCode values ('B','Y')
go
Insert into Pay_FormulaCode values ('C','Y')
go
Insert into Pay_FormulaCode values ('D','Y')
go
Insert into Pay_FormulaCode values ('E','Y')
go
Insert into Pay_FormulaCode values ('F','Y')
go
Insert into Pay_FormulaCode values ('G','Y')
go
Insert into Pay_FormulaCode values ('H','Y')
go
Insert into Pay_FormulaCode values ('I','Y')
go
Insert into Pay_FormulaCode values ('J','Y')
go
Insert into Pay_FormulaCode values ('K','Y')
go
Insert into Pay_FormulaCode values ('L','Y')
go
Insert into Pay_FormulaCode values ('M','Y')
go
Insert into Pay_FormulaCode values ('N','Y')
go
Insert into Pay_FormulaCode values ('O','Y')
go
Insert into Pay_FormulaCode values ('P','Y')
go
Insert into Pay_FormulaCode values ('Q','Y')
go
Insert into Pay_FormulaCode values ('R','Y')
go
Insert into Pay_FormulaCode values ('S','Y')
go
Insert into Pay_FormulaCode values ('T','Y')
go
Insert into Pay_FormulaCode values ('U','Y')
go
Insert into Pay_FormulaCode values ('V','Y')
go
Insert into Pay_FormulaCode values ('W','Y')
go
Insert into Pay_FormulaCode values ('X','Y')
go
Insert into Pay_FormulaCode values ('Y','Y')
go
Insert into Pay_FormulaCode values ('Z','Y')
go
Insert into Pay_FormulaCode values ('1','Y')
go
Insert into Pay_FormulaCode values ('2','Y')
go
Insert into Pay_FormulaCode values ('3','Y')
go
Insert into Pay_FormulaCode values ('4','Y')
go
Insert into Pay_FormulaCode values ('5','Y')
go
Insert into Pay_FormulaCode values ('6','Y')
go
Insert into Pay_FormulaCode values ('7','Y')
go
Insert into Pay_FormulaCode values ('8','Y')
go
Insert into Pay_FormulaCode values ('9','Y')
go
Drop procedure Pay_FormulaCode_SelectAll
go
Create procedure Pay_FormulaCode_SelectAll 
as    
Begin    
   SELECT Code,Flag FROM Pay_FormulaCode 
End
go