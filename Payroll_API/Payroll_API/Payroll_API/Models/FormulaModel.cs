﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using Payroll_API.DataAccess;

namespace Payroll_API.Models
{
    public class FormulaModel
    {
        public List<FormulaSetupModel.FormulaCode_list> GetFormulaCodeList()
        {
            List<FormulaSetupModel.FormulaCode_list> FormulaList = new List<FormulaSetupModel.FormulaCode_list>();
            DataTable dt = DataOperation.GetDataTableWithoutParameter("Pay_FormulaCode_SelectAll");
            if (dt.Rows.Count > 0)
            {
                for (int index = 0; index < dt.Rows.Count; index++)
                {
                    FormulaList.Add(new FormulaSetupModel.FormulaCode_list
                    {
                        FrmlCode = Convert.ToString(dt.Rows[index]["Code"]).Trim(),
                        FrmlFlag = Convert.ToString(dt.Rows[index]["Flag"]).Trim(),
                    });
                }
            }
            return FormulaList;
        }
    }
}