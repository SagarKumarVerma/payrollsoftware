﻿using Payroll_API.DataAccess;
using KellermanSoftware.CompareNetObjects;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Reflection;
using System.Web;
using System.IO;

namespace Payroll_API.Models
{
    public class AuditLog
    {
        public class AuditDelta
        {
            public string Field { get; set; }
            public string OldValue { get; set; }
            public string NewValue { get; set; }
        }
        public static void CreateAuditTrail(string modulename, string IdFieldName, string IdFieldValue,
                                string ChangeRemark, string userName, string userData, Object OldObject, Object NewObject)
        {
            // get the differance
            string username = userName.Trim();
            string userdata = userData.Trim();
            CompareLogic compObjects = new CompareLogic();
            compObjects.Config.MaxDifferences = 99;
            ComparisonResult compResult = compObjects.Compare(OldObject, NewObject);
            List<AuditDelta> DeltaList = new List<AuditDelta>();
            foreach (var change in compResult.Differences)
            {
                AuditDelta delta = new AuditDelta();

                delta.Field = change.PropertyName;
                delta.OldValue = change.Object1Value;
                delta.NewValue = change.Object2Value;
                DeltaList.Add(delta);

            }
            string Changes = "";
            if (ChangeRemark == AppConstant.CREATED)
            {
                Changes = "";
            }
            else
            {
                Changes = JsonConvert.SerializeObject(DeltaList);
            }

            int res = DataOperation.InsUpdDel("SP_AuditLogTrail", new SqlParameter("UserName", username.Trim()),
              new SqlParameter("UserData", userdata.Trim()),
              new SqlParameter("ModuleName", modulename.Trim()),
              new SqlParameter("IdFieldName", IdFieldName.Trim()),
              new SqlParameter("IdFieldValue", IdFieldValue.Trim()),
              new SqlParameter("ChangeRemark", ChangeRemark.Trim()),
              new SqlParameter("LogRemark", Changes.Trim()));
        }
        public static void writelog(string mystr)
        {
            StreamWriter writelog = new StreamWriter(HttpContext.Current.Server.MapPath("~/App_Data/log/log" + DateTime.Now.Year.ToString("d4") + DateTime.Now.Month.ToString("d2") + DateTime.Now.Day.ToString("d2") + ".txt"), true);
            writelog.Write(DateTime.Now.Year.ToString("d4") + DateTime.Now.Month.ToString("d2") + DateTime.Now.Day.ToString("d2") + DateTime.Now.Hour.ToString("d2") + DateTime.Now.Minute.ToString("d2") + DateTime.Now.Second.ToString("d2") + ":" + mystr);
            writelog.Close();
        }
    }
}