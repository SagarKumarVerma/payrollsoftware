﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Payroll_API.Models
{
    public static class AppConstant
    {
        #region ////***common use*****////
        public const string CREATED = "Created";
        public const string MODIFIED = "Modified";
        public const string DELETED = "Deleted";
        public const string JSONERRORLOGMSG = "error";
        public const string ERRORLOGMSG = "Error occured, contact to administrator";
        #endregion
    }
}