﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Payroll_API.Models
{
    public class BankCloneModel
    {
        public string BankCode { get; set; }
        public string BankName { get; set; }
        public string IFSCCode { get; set; }
        public string Address { get; set; }
        public string ShortName { get; set; }
    }
}