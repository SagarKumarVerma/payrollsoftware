﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.ComponentModel.DataAnnotations;
using System.Threading.Tasks;
using System.ComponentModel.DataAnnotations.Schema;
using System.ComponentModel;

namespace Payroll_API.Models
{
    public class FormulaSetupModel
    {
        #region ///****use for Formula Setup Master constant*******/////
        public const string FORMULASETUPMASTER = "Formula Setup Master";
        public const string FORMULASETUPCODE = "Formula Code";
        #endregion
        public List<FormulaCode_list> FormulaList { get; set; }
        public string FormulaCode { get; set; }
        public string Description { get; set; }
        public string Formula { get; set; }
        public string LastModifiedBy { get; set; }
        public DateTime CreatedDate { get; set; }
        public DateTime LastModifiedDate { get; set; }
        public string LoginTerminalNameIP { get; set; }
        public string AutoGeneratedFlag { get; set; }
        public class FormulaCode_list
        {
            public string FrmlCode { get; set; }
            public string FrmlFlag { get; set; }
        }
    }
}