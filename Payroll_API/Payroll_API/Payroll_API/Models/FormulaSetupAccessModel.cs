﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data;
using System.Data.SqlClient;
using System.Threading.Tasks;
using Payroll_API.DataAccess;

namespace Payroll_API.Models
{
    public class FormulaSetupAccessModel
    {
        public int IsCheckIsNumberExpID()
        {
            int res = 0;
            DataTable dt = DataOperation.GetDataTableWithoutParameter("Payroll_TblFormulaSetupMaster_CheckIsNumberExpID");
            if (dt.Rows.Count > 0)
            {
                for (int index = 0; index < dt.Rows.Count; index++)
                {
                    res = Convert.ToInt32(dt.Rows[index]["cnt"].ToString());
                }
            }
            return res;
        }
        //To View all FormulaSetup details    
        public IEnumerable<FormulaSetupModel> GetAllFormulaSetup()
        {
            DateTime date = System.DateTime.Now;
            List<FormulaSetupModel> lstFormulaSetup = new List<FormulaSetupModel>();
            DataTable dt = DataOperation.GetDataTableWithoutParameter("Payroll_TblFormulaSetupMaster_AllRec");
            if (dt.Rows.Count > 0)
            {
                for (int index = 0; index < dt.Rows.Count; index++)
                {
                    FormulaSetupModel FrmlStpModel = new FormulaSetupModel();
                    FrmlStpModel.FormulaCode = dt.Rows[index]["FormulaCode"].ToString();
                    FrmlStpModel.Description = dt.Rows[index]["Description"].ToString();
                    FrmlStpModel.Formula = dt.Rows[index]["Formula"].ToString();
                    FrmlStpModel.CreatedDate = Convert.ToDateTime(dt.Rows[index]["CreatedDate"].ToString());
                    FrmlStpModel.LastModifiedBy = dt.Rows[index]["LastModifiedBy"].ToString();
                    FrmlStpModel.LastModifiedDate = Convert.ToDateTime(dt.Rows[index]["LastModifiedDate"].ToString());
                    lstFormulaSetup.Add(FrmlStpModel);
                }
            }
            return lstFormulaSetup;
        }
        //To Add new FormulaSetup record    
        public int Add(FormulaSetupModel FrmlStpMst)
        {
            int res = DataOperation.InsUpdDel("Payroll_TblFormulaSetupMaster_Add",
                new SqlParameter("FormulaCode", FrmlStpMst.FormulaCode),
                new SqlParameter("Description", FrmlStpMst.Description),
                new SqlParameter("Formula", FrmlStpMst.Formula),
                new SqlParameter("LastModifiedBy", FrmlStpMst.LastModifiedBy.Trim()));
            if (res == 1)
            {
                FormulaSetupModel oldobject = GetFormulaSetupData(FrmlStpMst.FormulaCode);
                AuditLog.CreateAuditTrail(FormulaSetupModel.FORMULASETUPMASTER, FormulaSetupModel.FORMULASETUPCODE, FrmlStpMst.FormulaCode, AppConstant.CREATED, FrmlStpMst.LastModifiedBy, FrmlStpMst.LoginTerminalNameIP, oldobject, FrmlStpMst);
            }
            return res;
        }
        //To Update the records of a particluar FormulaSetup 
        public int Update(FormulaSetupModel FrmlStpMst)
        {
            FormulaSetupModel objgetexpmast = GetFormulaSetupData(FrmlStpMst.FormulaCode);
            int res = DataOperation.InsUpdDel("Payroll_TblFormulaSetupMaster_Upd",
                new SqlParameter("FormulaCode", FrmlStpMst.FormulaCode),
                new SqlParameter("Description", FrmlStpMst.Description),
                new SqlParameter("Formula", FrmlStpMst.Formula),
                new SqlParameter("LastModifiedBy", FrmlStpMst.LastModifiedBy.Trim()));
            if (res == 1)
            {
                FormulaSetupCloneModel objAuditold = new FormulaSetupCloneModel();
                FormulaSetupCloneModel objAuditnew = new FormulaSetupCloneModel();
                //code for clone copy create for old object
                objAuditold.FormulaCode = objgetexpmast.FormulaCode;
                objAuditold.Description = objgetexpmast.Description;
                objAuditold.Formula = objgetexpmast.Formula;
                //code for clone copy create for new  object
                objAuditnew.FormulaCode = FrmlStpMst.FormulaCode;
                objAuditnew.Description = FrmlStpMst.Description;
                objAuditnew.Formula = FrmlStpMst.Formula;

                //objAuditnew.LastModifiedBy = Convert.ToString(HttpContext.Current.Session["UserName"]);
                //objAuditnew.LastModifiedDate = DateTime.Now;
                AuditLog.CreateAuditTrail(FormulaSetupModel.FORMULASETUPMASTER, FormulaSetupModel.FORMULASETUPCODE, FrmlStpMst.FormulaCode, AppConstant.MODIFIED, FrmlStpMst.LastModifiedBy, FrmlStpMst.LoginTerminalNameIP, objAuditold, objAuditnew);
            }
            return res;
        }
        //Get the details of a particular FormulaSetup Data  
        public FormulaSetupModel GetFormulaSetupData(string FormulaCode)
        {

            FormulaSetupModel FrlmStpMstMod = new FormulaSetupModel();
            DataTable dt = DataOperation.GetDataTableWithParameter("Payroll_TblFormulaSetupMaster_GetFormulaSetupData", new SqlParameter("FormulaCode", FormulaCode.Trim()));
            if (dt.Rows.Count > 0)
            {
                for (int index = 0; index < dt.Rows.Count; index++)
                {
                    FrlmStpMstMod.FormulaCode = dt.Rows[index]["FormulaCode"].ToString();
                    FrlmStpMstMod.Description = dt.Rows[index]["Description"].ToString();
                    FrlmStpMstMod.Formula = dt.Rows[index]["Formula"].ToString();
                    FrlmStpMstMod.CreatedDate = Convert.ToDateTime(dt.Rows[index]["CreatedDate"].ToString());
                    FrlmStpMstMod.LastModifiedBy = dt.Rows[index]["LastModifiedBy"].ToString();
                    FrlmStpMstMod.LastModifiedDate = Convert.ToDateTime(dt.Rows[index]["LastModifiedDate"].ToString());
                }
            }
            return FrlmStpMstMod;
        }
        //To Delete the record on a particular  FormulaSetup  
        public int Deleteold(string FormulaCode)
        {
            string oldCode = FormulaCode;
            FormulaSetupModel oldobject = GetFormulaSetupData(oldCode.Trim());
            FormulaSetupCloneModel objAuditold = new FormulaSetupCloneModel();
            FormulaSetupCloneModel objAuditnew = new FormulaSetupCloneModel();
            int res = DataOperation.InsUpdDel("Payroll_TblFormulaSetupMaster_Del", new SqlParameter("FormulaCode", FormulaCode.Trim()));
            return res;
        }
        public int Delete(FormulaSetupModel FrmlStpMst)
        {
            int res = -1;
            FormulaSetupCloneModel objAuditold = new FormulaSetupCloneModel();
            FormulaSetupCloneModel objAuditnew = new FormulaSetupCloneModel();
            FormulaSetupModel objgetexpmast = GetFormulaSetupData(FrmlStpMst.FormulaCode.Trim());
            //code for clone copy create for old object
            objAuditold.FormulaCode = objgetexpmast.FormulaCode;
            objAuditold.Description = objgetexpmast.Description;
            objAuditold.Formula = objgetexpmast.Formula;

            //code for clone copy create for new  object
            objAuditnew.FormulaCode = "";
            objAuditnew.Description = "";
            objAuditnew.Formula = "";

            res = DataOperation.InsUpdDel("Payroll_TblFormulaSetupMaster_Del", new SqlParameter("FormulaCode", FrmlStpMst.FormulaCode.Trim()));
            if (res == 1)
            {
                AuditLog.CreateAuditTrail(FormulaSetupModel.FORMULASETUPMASTER, FormulaSetupModel.FORMULASETUPCODE, FrmlStpMst.FormulaCode.Trim(), AppConstant.DELETED, FrmlStpMst.LastModifiedBy, FrmlStpMst.LoginTerminalNameIP, objAuditold, objAuditnew);
            }
            // }
            return res;
        }
    }
}