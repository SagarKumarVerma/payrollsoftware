﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Data;
using System.Data.SqlClient;
using System.Configuration;
using System.Web.Helpers;
using Payroll_API.Models;
using Payroll_API.DataAccess;

namespace Payroll_API.Controllers
{
    public class BankController : ApiController
    {
        BankAccessModel objBank = new BankAccessModel();

        [Route("Bank/GetSpecRec")]
        [HttpPost]
        public IHttpActionResult GetbyCode([FromBody]BankModel BankMst)
        {
            try
            {
                var BankMstVar = objBank.GetBankData(BankMst.BankCode.Trim());
                return Ok(BankMstVar);
            }
            catch (Exception ex)
            {
                ExceptionLogging.SendErrorToText(ex);
                return BadRequest();
            }

        }

        [Route("Bank/GetBankcode")]
        [HttpPost]
        public IHttpActionResult GetbyBankcode([FromBody]BankModel BankMst)
        {
            try
            {
                int result = 0;
                result = objBank.GetBankCode(BankMst.BankCode.Trim());
                if (result == 0)
                {
                    return Ok();
                }
                else if (result > 0)
                {
                    return Ok(1);
                }
                else
                {
                    return BadRequest();
                }
            }
            catch (Exception ex)
            {
                ExceptionLogging.SendErrorToText(ex);
                return BadRequest();
            }
        }
        [Route("Bank/GetAllList")]
        [HttpPost]
        public IHttpActionResult ListAllBank([FromBody]BankModel BankMst)
        {
            try
            {
                return Ok(objBank.GetAllBank());
            }
            catch (Exception ex)
            {
                ExceptionLogging.SendErrorToText(ex);
                return BadRequest();
            }

        }
        [Route("Bank/InsertRecord")]
        [HttpPost]
        public IHttpActionResult Add([FromBody]BankModel BankMst)
        {
            try
            {
                int result = 0;
                result = objBank.Add(BankMst);
                if (result > 0)
                {
                    return Ok();
                }
                else
                {
                    return BadRequest();
                }
            }
            catch (Exception ex)
            {
                ExceptionLogging.SendErrorToText(ex);

                return BadRequest();
            }

        }
        [Route("Bank/UpdateRecord")]
        [HttpPost]
        public IHttpActionResult Update([FromBody]BankModel BankMst)
        {
            try
            {
                int result = 0;
                result = objBank.Update(BankMst);
                if (result > 0)
                {
                    return Ok();
                }
                else
                {
                    return BadRequest();
                }
            }
            catch (Exception ex)
            {
                ExceptionLogging.SendErrorToText(ex);
                return BadRequest();
            }
        }
        [Route("Bank/DeleteRecord")]
        [HttpPost]
        public IHttpActionResult Delete([FromBody]BankModel BankMst)
        {
            int result = 0;
            try
            {
                result = objBank.Delete(BankMst);
                if (result > 0)
                {
                    return Ok();
                }
                else
                {
                    return BadRequest();
                }
            }
            catch (Exception ex)
            {
                ExceptionLogging.SendErrorToText(ex);
                return BadRequest();
            }

        }
    }
}
