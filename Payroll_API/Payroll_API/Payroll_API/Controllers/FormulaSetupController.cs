﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Data;
using System.Data.SqlClient;
using System.Configuration;
using System.Web.Helpers;
using Payroll_API.Models;
using Payroll_API.DataAccess;

namespace Payroll_API.Controllers
{
    public class FormulaSetupController : ApiController
    {
        FormulaSetupAccessModel objFormulaSetup = new FormulaSetupAccessModel();

        [Route("FormulaSetup/GetSpecRec")]
        [HttpPost]
        public IHttpActionResult GetbyCode([FromBody]FormulaSetupModel FrmlStpMst)
        {
            try
            {
                var CateMstVar = objFormulaSetup.GetFormulaSetupData(FrmlStpMst.FormulaCode.Trim());
                return Ok(CateMstVar);
            }
            catch (Exception ex)
            {
                ExceptionLogging.SendErrorToText(ex);
                return BadRequest();
            }

        }
        [Route("FormulaSetup/GetFormulaCodeList")]
        [HttpPost]
        public IHttpActionResult ListAllFormulaCode([FromBody]FormulaSetupModel FrmlStpMst)
        {
            try
            {
                FormulaModel FrmlMdl = new FormulaModel();
                return Ok(FrmlMdl.GetFormulaCodeList());
            }
            catch (Exception ex)
            {
                ExceptionLogging.SendErrorToText(ex);
                return BadRequest();
            }

        }
        [Route("FormulaSetup/GetAllList")]
        [HttpPost]
        public IHttpActionResult ListAllFormulaSetup([FromBody]FormulaSetupModel FrmlStpMst)
        {
            try
            {
                return Ok(objFormulaSetup.GetAllFormulaSetup());
            }
            catch (Exception ex)
            {
                ExceptionLogging.SendErrorToText(ex);
                return BadRequest();
            }

        }
        [Route("FormulaSetup/InsertRecord")]
        [HttpPost]
        public IHttpActionResult Add([FromBody]FormulaSetupModel FrmlStpMst)
        {
            try
            {
                int result = 0;
                result = objFormulaSetup.Add(FrmlStpMst);
                if (result > 0)
                {
                    return Ok();
                }
                else
                {
                    return BadRequest();
                }
            }
            catch (Exception ex)
            {
                ExceptionLogging.SendErrorToText(ex);

                return BadRequest();
            }

        }
        [Route("FormulaSetup/UpdateRecord")]
        [HttpPost]
        public IHttpActionResult Update([FromBody]FormulaSetupModel FrmlStpMst)
        {
            try
            {
                int result = 0;
                result = objFormulaSetup.Update(FrmlStpMst);
                if (result > 0)
                {
                    return Ok();
                }
                else
                {
                    return BadRequest();
                }
            }
            catch (Exception ex)
            {
                ExceptionLogging.SendErrorToText(ex);
                return BadRequest();
            }
        }
        [Route("FormulaSetup/DeleteRecord")]
        [HttpPost]
        public IHttpActionResult Delete([FromBody]FormulaSetupModel FrmlStpMst)
        {
            int result = 0;
            try
            {
                result = objFormulaSetup.Delete(FrmlStpMst);
                if (result > 0)
                {
                    return Ok();
                }
                else
                {
                    return BadRequest();
                }
            }
            catch (Exception ex)
            {
                ExceptionLogging.SendErrorToText(ex);
                return BadRequest();
            }

        }
    }
}
